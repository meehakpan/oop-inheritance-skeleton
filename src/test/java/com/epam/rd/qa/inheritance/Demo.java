package com.epam.rd.qa.inheritance;

import java.math.BigDecimal;

public class Demo {

    public static void main(String[] args) {

        Employee emp1 = new Employee("John Doe", BigDecimal.valueOf(200));
        Employee emp2 = new Employee("Elion Fraser", BigDecimal.valueOf(150));

        Employee[] emps = new Employee[3];
        emps[0] = emp1;
        emps[1] = emp2;;
        emps[2] = new Employee("Grace Ansel", BigDecimal.valueOf(100));;

        for(int i = 0; i < emps.length; i++){
            System.out.println(emp2);

        }

    }
}
