package com.epam.rd.qa.inheritance;

import static org.mockito.ArgumentMatchers.doubleThat;
import static org.mockito.ArgumentMatchers.intThat;
import java.math.BigDecimal;
public class Company {
    private Employee[] employees;
    public Company(Employee[] employees) {
        if (employees == null) {
            throw new IllegalArgumentException();
        }else{
            for (Employee employee : employees) {
                if (employee == null) {
                    throw new IllegalArgumentException();
                }else {
                    this.employees=employees;
                }
            }
        }
    }
    public void giveEverybodyBonus(BigDecimal companyBonus) {
        for (Employee employee : employees) {
            employee.setBonus(companyBonus);
        }
    }
    public BigDecimal totalToPay() {
        double sum = 0.0;
        for (Employee employee : employees) {
            sum += Double.parseDouble(employee.toPay().toString());
        }
        BigDecimal totalSum = new BigDecimal(sum);
        return totalSum;
    }
    public String nameMaxSalary() {
        double maxSalary = 0;
        String name = null;
        for (Employee employee : employees) {
            if(maxSalary == Double.parseDouble(employee.toPay().toString()));
            name = employee.getName();
        }
        return name;
    }
    {
    }
}