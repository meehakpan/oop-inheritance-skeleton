package com.epam.rd.qa.inheritance;

import java.math.BigDecimal;

import java.math.BigDecimal;
public class SalesPerson extends Employee{
    private final int percent;
    public SalesPerson(String name, BigDecimal salary, int percent) {
        super(name, salary);
        if (percent < 0) {
            throw new IllegalArgumentException();
        }else {
            this.percent = percent;
        }
    }
    public void setBonus(BigDecimal bonus) {
        if (percent>200) {
            double bonusDbl = Double.parseDouble(bonus.toString());
            bonus = new BigDecimal(bonusDbl*3);
        }if (percent>100 && percent<=200) {
            double bonusDbl = Double.parseDouble(bonus.toString());
            bonus = new BigDecimal(bonusDbl*2);
        }
        super.setBonus(bonus);
    }
}

