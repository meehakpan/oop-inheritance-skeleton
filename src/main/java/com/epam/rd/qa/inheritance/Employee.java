package com.epam.rd.qa.inheritance;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Employee {
        private final String name;
        private final BigDecimal salary;
        private BigDecimal bonus;

        public Employee(String name, BigDecimal salary) {
            double salaryDbl = Double.parseDouble(salary.toString());
            if (salaryDbl > 0) {
                this.salary = salary;
            } else {
                throw new IllegalArgumentException();
            }
            if (name != null) {
                this.name = name;
            } else {
                throw new IllegalArgumentException();
            }
        }

        public String getName() {
            return name;
        }

        public BigDecimal getSalary(BigDecimal salary) {
            double salaryDbl = Double.parseDouble(salary.toString());
            if (salaryDbl > 0) {
                return salary;
            } else {
                throw new IllegalArgumentException();
            }
        }

        public void setBonus(BigDecimal bonus) {
            double bonusDbl = Double.parseDouble(bonus.toString());
            if (bonusDbl > 0) {
                this.bonus = bonus;
            } else {
                throw new IllegalArgumentException();
            }
        }

        public BigDecimal toPay() {
            double bonusDbl = Double.parseDouble(bonus.toString());
            double salaryDbl = Double.parseDouble(salary.toString());
            double totalPayDbl = bonusDbl + salaryDbl;
            BigDecimal totalPay = new BigDecimal(totalPayDbl);
            return totalPay;
        }
    }
