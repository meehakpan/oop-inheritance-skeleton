package com.epam.rd.qa.inheritance;

import java.math.BigDecimal;

public class Manager extends Employee{
    private final int clientAmount;

    public Manager(String name, BigDecimal salary, int clientAmount) {
        super(name, salary);
        if (clientAmount<0) {
            throw new IllegalArgumentException();
        }else {
            this.clientAmount = clientAmount;
        }
    }
    public void setBonus(BigDecimal bonus) {
        if (clientAmount>150) {
            double bonusDbl = Double.parseDouble(bonus.toString());
            bonus = new BigDecimal(bonusDbl+1000);
        }if (clientAmount>100 && clientAmount<=150) {
            double bonusDbl = Double.parseDouble(bonus.toString());
            bonus = new BigDecimal(bonusDbl+500);
        }
        super.setBonus(bonus);
    }
}